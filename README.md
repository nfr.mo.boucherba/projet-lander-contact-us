# Creer un wireframe et le developper en HTML/CSS
PP
Philippe Pereira

09.11.2021

# Reproduire une maquette en wireframe et la convertir en HTML et CSS

Vous allez reproduire une maquette d'une page de contact en wireframe, que vous devrez ensuite développer en HTML et CSS en vous appuyant sur les spécifications fournies.

# Contexte du projet
Le propriétaire de la société Lander App souhaite mettre à jour sa page de contact. Il vous fournit la maquette de la dite page que vous pouvez récupérer des pièces jointes et vous demande de :

Reproduire la maquette en wireframe avec l'outil de votre choix.
Creer le wireframe version mobile
Analyser la maquette et distinguer ses principaux blocs.
Traduire la maquette en code HTML et CSS en respectant les consignes suivantes :
Utiliser les balises sémantiques de l'HTML5 (header, main, footer, nav...);
Implémenter la bibliothèque Font Awesome pour l'incorporation des différentes icones des réseaux sociaux;
Appliquer un effet de survol sur les boutons;